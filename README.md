This is a customised version of the Gitlab Runner Helm chart. It contains some
community contributions to allow multiple runners and also removes a lot of
complexity to ease future maintenance.

This is in light of Gitlab's decision to focus efforts on the Gitlab Runner
Operator instead.

Documents:

* [License](gitlab-runner/LICENSE)
* [Notice](gitlab-runner/NOTICE)

Credits:

* [Gitlab] - Original creators of the Helm chart.
* [Thorsten Banhart] - Second pass at multiple runners.
* [Guilherme Torres Castro] - Original pass at multiple runners.

<!---------------------------------------------------------------------------->

[Gitlab]:                  https://gitlab.com/
[Thorsten Banhart]:        https://gitlab.com/banhartt
[Guilherme Torres Castro]: https://gitlab.com/guitcastro

