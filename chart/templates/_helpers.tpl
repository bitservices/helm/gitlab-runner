{{/* vim: set filetype=mustache: */}}
{{/* --------------------------------------------------------------------- */}}
{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "gitlab-runner.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{/* --------------------------------------------------------------------- */}}
{{/*
Generate standard set of labels to be used throughout the chart.
*/}}
{{- define "gitlab-runner.labels" -}}
{{- range $k, $v := .Values.labels }}
{{ $k }}: "{{ tpl $v $ }}"
{{- end -}}
{{- end -}}
{{/* ---------------------------------------------------------------------- */}}
{{/*
Generate standard selector to be used throughout the chart.
*/}}
{{- define "gitlab-runner.selector" -}}
{{- range $k, $v := .Values.selector }}
{{ $k }}: "{{ tpl $v $ }}"
{{- end -}}
{{- end -}}
{{/* ---------------------------------------------------------------------- */}}
{{/*
Generate standard affinity to be used throughout the chart.
*/}}
{{- define "gitlab-runner.affinity" -}}
{{- range $k, $v := .Values.labels }}
- key: "{{ $k }}"
  operator: In
  values:
    - "{{ tpl $v $ }}"
{{- end -}}
{{- end -}}
{{/* ---------------------------------------------------------------------- */}}
