#!/bin/bash -e
###############################################################################

set -o pipefail

###############################################################################

{{- if and .Values.runnerToken (or (not (eq .Values.replicas 1.0)) .Values.autoscaling.enabled) }}
{{- fail "Using a runner token with more than 1 replica or autoscaling is not supported." }}
{{- end }}

###############################################################################

mkdir -p "${HOME}/.gitlab-runner"
cp -v "/configmaps/config.toml" "${HOME}/.gitlab-runner"

###############################################################################

bash "/configmaps/pre-registration-script.sh"
bash "/configmaps/register-the-runners.sh"
bash "/configmaps/post-registration-script.sh"

###############################################################################

exec "/entrypoint" \
  run \
  --user="gitlab-runner" \
  --working-directory="${HOME}"

###############################################################################
