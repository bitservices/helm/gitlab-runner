###############################################################################

concurrent     = {{ .Values.config.concurrent    }}
check_interval = {{ .Values.config.checkInterval }}

###############################################################################

log_level  = "{{ .Values.config.log.level  }}"
log_format = "{{ .Values.config.log.format }}"

###############################################################################

{{- if .Values.metrics.enabled }}
listen_address = ':{{ .Values.metrics.port }}'
{{- end }}

###############################################################################

{{- if .Values.config.sentryDsn }}
sentry_dsn = "{{ .Values.config.sentryDsn }}"
{{- end }}

###############################################################################

