#!/bin/bash -e
###############################################################################

set -o pipefail

###############################################################################

MAX_REGISTER_ATTEMPTS=5
REGISTER_SLEEP_INTERVAL=10

###############################################################################

{{- range $index, $runner := .Values.runners }}
for i in $(seq 1 "${MAX_REGISTER_ATTEMPTS}"); do
  echo "Registration attempt: ${i} of: ${MAX_REGISTER_ATTEMPTS}"

  if [ -f "/secrets/runner-registration-token-{{ $index }}" ]; then
    export REGISTRATION_TOKEN="$(cat "/secrets/runner-registration-token-{{ $index }}")"
  fi

  set +e

  "/entrypoint" \
    register \
    --template-config "/configmaps/config.templates.toml-{{ $index }}" \
    --executor "{{ default "kubernetes" $runner.executor }}" \
{{- if and (hasKey $runner "name") $runner.name }}
    --name "{{ tpl $runner.name $ }}" \
{{- end }}
{{- if $runner.envVars }}
{{- range $key, $value := $runner.envVars }}
    --env "{{ $key }}={{ $value }}"
{{- end }}
{{- end }}
{{- if or (not (hasKey $runner "locked")) $runner.locked }}
    --locked \
{{- end }}
{{- if and (hasKey $runner "protected") $runner.protected }}
    --access-level "ref_protected" \
{{- end }}
{{- if and (hasKey $runner "runUntagged") (eq (tpl (toString $runner.runUntagged) $) "true") }}
    --run-untagged \
{{- end }}
{{- if $runner.serviceAccountName }}
    --kubernetes-service-account "{{ $runner.serviceAccountName }}" \
{{- end }}
{{- if $runner.tags }}
    --tag-list "{{ tpl $runner.tags $ }}" \
{{- end }}
{{- if $runner.config }}
{{- if (not (regexMatch "\\s*namespace\\s*=" $runner.config)) }}
    --kubernetes-namespace "{{ $.Release.Namespace }}" \
{{- end }}
{{- end }}
{{- if $runner.cacheSecretName | hasPrefix "s3access" }}
    --cache-s3-access-key "$(cat "/secrets/{{ $runner.cacheSecretName }}-{{ $index }}/accesskey")" \
    --cache-s3-secret-key "$(cat "/secrets/{{ $runner.cacheSecretName }}-{{ $index }}/secretkey")" \
{{- end }}
{{- if $runner.cacheSecretName | hasPrefix "google-application-credentials" }}
    --cache-gcs-credentials-file "/secrets/{{ $runner.cacheSecretName -}}-{{ $index }}" \
{{- end }}
{{- if $runner.cacheSecretName | hasPrefix "gcsaccess" }}
    --cache-gcs-access-id "$(cat "/secrets/{{ $runner.cacheSecretName }}-{{ $index }}/gcs-access-id")" \
    --cache-gcs-private-key "$(cat "/secrets/{{ $runner.cacheSecretName }}-{{ $index }}/gcs-private-key")" \
{{- end }}
{{- if $runner.cacheSecretName | hasPrefix "azureaccess" }}
    --cache-azure-account-name "$(cat "/secrets/{{ $runner.cacheSecretName }}-{{ $index }}/azure-account-name")" \
    --cache-azure-account-key "$(cat "/secrets/{{ $runner.cacheSecretName }}-{{ $index }}/azure-account-key")" \
{{- end}}
    --non-interactive

  retval=${?}

  if [ ${retval} = 0 ]; then
    break
  elif [ ${i} = ${MAX_REGISTER_ATTEMPTS} ]; then
    exit 1
  fi

  set -e

  unset REGISTRATION_TOKEN
  sleep ${REGISTER_SLEEP_INTERVAL}
done
{{- end}}

###############################################################################
